package eprecise.libs.faces.dataSelector;

import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;

import org.primefaces.component.autocomplete.AutoComplete;

@FacesComponent("dataSelector")
public class DataSelector extends UIInput implements NamingContainer {
    
    private AutoComplete selector;
    
    @Override
    public String getFamily() {
	return UINamingContainer.COMPONENT_FAMILY;
    }
    
    public AutoComplete getSelector() {
	return this.selector;
    }
    
    public void setSelector(AutoComplete selector) {
	this.selector = selector;
    }
    
}