function expandsMenu() {
	var _menu = $('.menu');
	var _menuHover = $('.menu-hover');
	var _divOutput = $('.div-output');
	var _fieldsOutput = $('iframe, .div-output, .contentSide');
	var _timeout = null;

	function layerUp() {
		_divOutput.css('z-index', '10004');
		_menu.css('z-index', '10005');
	}

	function layerDown() {
		if (!_menu.data('overall')) {
			_menu.css('z-index', '15');
			_divOutput.css('z-index', '14');
		}
	}

	var _expand = function(evt) {
		var menu = $('.menu');
		var opening = menu.is(':animated');
		if (!menu.data('expanded') && !opening) {
			layerUp();
			_menu.animate({
				width : '255px',
			}, 100, function() {
				if (!menu.data('expanded')) {
					_divOutput.show();
					_menu.addClass('expanded');
					_menu.data('expanded', true);
				}
			});
			_menuHover.animate({
				opacity : '1'
			});
		}
	};

	var _collapse = function(evt) {
		_divOutput.hide();
		if (!_menu.data('expanded')) {
			return;
		}

		_menu.removeClass('expanded');
		_menu.animate({
			width : '50px'
		}, 100, function() {
			if (_menu.data('expanded', true)) {
				_divOutput.hide();
				_menu.data('expanded', false);
			}
		});

		_menuHover.animate({
			opacity : '0.8'
		});

		layerDown();
	};

	_menu.mouseover(function(evt) {
		if (!_timeout) {
			layerUp();
			_divOutput.show();
			_timeout = setTimeout(_expand, 300);
		}
	});

	_fieldsOutput.mouseover(function() {
		clearTimeout(_timeout);
		_timeout = null;
		layerDown();
		_collapse();
	});
}
$(document).ready(expandsMenu);
